INTRODUCTION
------------

Provides notification management system manageable from the backoffice

 * For a full description of the module, visit the project page:
   https://drupal.org/project/notification_template


MAINTAINERS
-----------

Current maintainers:

 * eme (https://www.drupal.org/u/eme)