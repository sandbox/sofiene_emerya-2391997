<?php
/**
 * @file
 * notification_template_feature.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function notification_template_feature_default_rules_configuration() {
  $items = array();
  $items['rules_email_activated'] = entity_import('rules_config', '{ "rules_email_activated" : {
      "LABEL" : "Account activation",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_update" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "account:status" ], "value" : "1" } },
        { "data_is" : { "data" : [ "account-unchanged:status" ], "value" : "0" } }
      ],
      "DO" : []
    }
  }');
  $items['rules_email_admin_created'] = entity_import('rules_config', '{ "rules_email_admin_created" : {
      "LABEL" : "Welcome (new user created by administrator)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "notification_template" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "2" : "2" } } } },
        { "data_is" : { "data" : [ "account:status" ], "value" : "1" } }
      ],
      "DO" : [
        { "notification_template_user" : { "notification" : "email_admin_created", "account" : [ "account" ] } }
      ]
    }
  }');
  $items['rules_email_blocked'] = entity_import('rules_config', '{ "rules_email_blocked" : {
      "LABEL" : "Account blocked",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "notification_template" ],
      "ON" : { "user_update" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "account:status" ], "value" : "0" } },
        { "data_is" : { "data" : [ "account-unchanged:status" ], "value" : "1" } }
      ],
      "DO" : [
        { "notification_template_user" : { "notification" : "email_blocked", "account" : [ "account" ] } }
      ]
    }
  }');
  $items['rules_email_no_approval_required'] = entity_import('rules_config', '{ "rules_email_no_approval_required" : {
      "LABEL" : "Welcome (no approval required)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_condition_site_variable", "notification_template" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "1" : "1" } } } },
        { "NOT rules_condition_site_variable_compare_variable" : { "variable_name" : "user_register", "variable_value" : "2" } }
      ],
      "DO" : [
        { "notification_template_user" : {
            "notification" : "email_no_approval_required",
            "account" : [ "account" ]
          }
        }
      ]
    }
  }');
  $items['rules_email_pending_approval'] = entity_import('rules_config', '{ "rules_email_pending_approval" : {
      "LABEL" : "Welcome (awaiting approval)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_condition_site_variable", "notification_template" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "1" : "1" } } } },
        { "data_is" : { "data" : [ "account:status" ], "value" : "0" } },
        { "rules_condition_site_variable_compare_variable" : { "variable_name" : "user_register", "variable_value" : "2" } }
      ],
      "DO" : [
        { "notification_template_user" : { "notification" : "email_pending_approval", "account" : [ "account" ] } }
      ]
    }
  }');
  return $items;
}
