<?php

function exporter_notifications($form, $form_state) {
  $form['fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tous les notifications'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
  );
  if (!empty($form_state['input']['notifications'])) {
    $notifications = $form_state['input']['notifications'];
    $array = array();
    foreach ($notifications as $machine_name) {
      $element = notification_template_load_by_machine_name($machine_name);
      unset($element->ntid);
      $array[$machine_name] = (array) $element;
    }
    $data = '$data = ' . var_export($array, TRUE) . ';';
    $form['fieldset']['data'] = array(
        '#type' => 'container',
        '0' => array('#markup' => $data),
    );
    dsm($form);
    return $form;
  }
  $list = notification_template_load_all();
  $options = array();
  foreach ($list as $notification) {
    $options[$notification->machine_name] = $notification->title;
  }
  if (!empty($list)) {
    $form['fieldset']['notifications'] = array(
        '#type' => 'checkboxes',
        '#options' => $options,
        '#required' => TRUE
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Export')
    );
  }
  return $form;
}

function notification_templates_test_sending($form, $form_state) {
  $list = notification_template_load_all();
  $entity_info = entity_get_info();
  $options = array(
      '' => 'choisir le type d\'entité'
  );
  foreach ($entity_info as $key => $tableau) {
    $options [$key] = $tableau ['label'];
  }
  if (!empty($list)) {
    $form ['vertical_tabs'] = array(
        '#type' => 'vertical_tabs'
    );
    foreach ($list as $notification) {
      $form [$notification->machine_name] = array(
          '#type' => 'fieldset',
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#title' => $notification->title,
          '#group' => 'vertical_tabs'
      );
      $form [$notification->machine_name] [$notification->machine_name . '_test'] = array(
          '#type' => 'checkbox',
          '#title' => t('Tester')
      );
      $form [$notification->machine_name] [$notification->machine_name . '_entity_type'] = array(
          '#type' => 'select',
          '#title' => t("Type d'entité"),
          '#options' => $options
      );
      $form [$notification->machine_name] [$notification->machine_name . '_id'] = array(
          '#type' => 'textfield',
          '#title' => t('ID')
      );
    }
    $form ['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Tester')
    );
    $form ['#submit'] [] = 'notification_templates_test_sending_form_submit';
    return $form;
  }
  else {
    $form ['markup'] = array(
        '#markup' => t('Aucune notification trouvé')
    );
    return $form;
  }
}

function notification_templates_test_sending_form_submit($form, $form_state) {
  $values = $form_state ['values'];
  $list = notification_template_load_all();
  $data = array();
  foreach ($list as $notification) {
    if (!empty($values [$notification->machine_name . '_test'])) {
      if (!empty($values [$notification->machine_name . '_entity_type']) && !empty($values [$notification->machine_name . '_id'])) {
        $entity_type = $values [$notification->machine_name . '_entity_type'];
        $id = array(
            $values [$notification->machine_name . '_id']
        );
        $entity = entity_load($entity_type, $id);
        if ($entity) {
          $data ['tokens'] [$entity_type] = array_shift($entity);
        }
      }
      notification_template_send_notification($notification->machine_name, NULL, $data);
    }
  }
}

/**
 * configuration des email à désactiver
 */
function notification_templates_settings($form, $form_state) {
  $form ['fieldset'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t("Desactivr l'envoi de mail pour ces modules")
  );
  $modules = module_implements('mail');
  $form ['fieldset'] ['notification_template_modules_mail_disables'] = array(
      '#title' => t('Modules'),
      '#type' => 'checkboxes',
      '#options' => drupal_map_assoc($modules),
      '#default_value' => variable_get('notification_template_modules_mail_disables', array()),
      '#multiple' => TRUE
  );
  $form ['fieldset'] ['notification_template_modules_mail_disables_show_message_status'] = array(
      '#title' => t('Afficher le message status'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('notification_template_modules_mail_disables_show_message_status', TRUE)
  );

  return system_settings_form($form);
}

// drupal_mail($module, $key, $to, $language);
/**
 * formulaire de traduction des notification
 */
function language_switcher($form, $form_state) {
  global $language;
  $language_list = language_list();
  $options = array();
  foreach ($language_list as $lang) {
    if ($lang->enabled == 1) {
      $options [$lang->language] = $lang->name;
    }
  }
  if (!empty($options)) {
    $form ['language'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#title' => t('Traduire en '),
        '#attributes' => array(
            'onchange' => 'this.form.submit();'
        ),
        '#default_value' => $language->language
    );
    $form ['#submit'] [] = 'language_switcher_submit';
    $form ['submit'] = array(
        '#type' => 'submit',
        '#value' => t('changer'),
        '#attributes' => array(
            'style' => 'display:none'
        )
    );
  }

  return $form;
}

function language_switcher_submit($form, $form_state) {
  $langue = $form_state ['values'] ['language'];
  $url = 'http://' . $_SERVER ['HTTP_HOST'] . base_path() . $langue . '/admin/config/notification-templates';
  drupal_goto($url);
}

/**
 * formulaire d'administration des notification
 */
function admin_list_notification($form, $form_state) {
  $list = notification_template_load_all();
  $language_switcher = drupal_get_form('language_switcher');
  if (!empty($language_switcher)) {
    $form ['#prefix'] = drupal_render($language_switcher);
  }
  if (!empty($list)) {
    $form ['vertical_tabs'] = array(
        '#type' => 'vertical_tabs'
    );
    foreach ($list as $notification) {
      $form [$notification->machine_name] = array(
          '#type' => 'fieldset',
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#title' => $notification->title,
          '#group' => 'vertical_tabs'
      );
      $class = $notification->ntid < 9 ? 'element-invisible' : '';
      $form [$notification->machine_name] [$notification->machine_name . '_delete'] = array(
          '#type' => 'checkbox',
          '#title' => t('Supprimer'),
      		'#title_display' => $notification->ntid > 8,
          '#access' => user_access('supprimer des notification'),
      		'#attributes' => array('class' => array($class))
      );
      $form [$notification->machine_name] [$notification->machine_name . '_ntid'] = array(
          '#type' => 'hidden',
          '#value' => $notification->ntid
      );
      $form [$notification->machine_name] [$notification->machine_name . '_title'] = array(
          '#type' => 'textfield',
          '#title' => t('Titre interne'),
          '#required' => TRUE,
          '#default_value' => $notification->title
      );
      $form [$notification->machine_name] [$notification->machine_name . '_machine_name'] = array(
          '#type' => 'machine_name',
          '#title' => t("Machine Name"),
          '#required' => TRUE,
          '#attributes' => array(
              'readOnly' => TRUE
          ),
          '#maxlength' => 120,
          '#machine_name' => array(
              'exists' => 'notification_template_name_exists',
              'source' => array(
                  $notification->machine_name,
                  'title'
              )
          ),
          '#default_value' => $notification->machine_name
      );
      $form [$notification->machine_name] [$notification->machine_name . '_from_mail'] = array(
          '#type' => 'textfield',
          '#title' => t('From email'),
          '#required' => TRUE,
          '#default_value' => $notification->from_mail
      );
      //
      $form [$notification->machine_name] [$notification->machine_name . '_email_destination'] = array(
          '#type' => 'textfield',
          '#title' => t('To Email'),
          '#default_value' => $notification->email_destination
      );
      $form [$notification->machine_name] [$notification->machine_name . '_object'] = array(
          '#type' => 'textfield',
          '#title' => t('Objet'),
          '#required' => TRUE,
          '#default_value' => $notification->object
      );
      $form [$notification->machine_name] [$notification->machine_name . '_notification'] = array(
          '#type' => 'text_format',
          '#title' => t('Corps'),
          '#required' => TRUE,
          '#format' => $notification->notification_format,
          '#default_value' => $notification->notification
      );
      $form [$notification->machine_name] [$notification->machine_name . '_active'] = array(
          '#type' => 'checkbox',
          '#title' => t('Active'),
          '#default_value' => $notification->active
      );
    }
    $form ['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    $form ['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    $form ['token_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('Tokens disponible'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#access' => user_access('voir les tokens')
    );
    $form ['token_fieldset'] ['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array(
            'node',
            'user',
            'entityform_type',
            'entityform'
        ),
        '#global_types' => TRUE,
        '#click_insert' => TRUE
    );
    return $form;
  }
  else {
    $form ['markup'] = array(
        '#markup' => t('Aucune notification trouvé')
    );
    return $form;
  }
}

function admin_list_notification_submit($form, $form_state) {
  $list = notification_template_load_all();
  $values = $form_state ['values'];
  foreach ($list as $notification) {
    $machine_name = $notification->machine_name;
    if (!empty($values [$machine_name . '_delete'])) {
      db_api_delete($notification, 'notification_template');
      continue;
    }
    $notification->title = $values [$machine_name . '_title'];
    $notification->from_mail = $values [$machine_name . '_from_mail'];
    $notification->notification_format = $values [$machine_name . '_notification'] ['format'];
    $notification->active = $values [$machine_name . '_active'];
    $notification->email_destination = $values [$machine_name . '_email_destination'];
    $notification->notification = $values [$machine_name . '_notification'] ['value'];
    $notification->object = $values [$machine_name . '_object'];
    db_api_save($notification, 'notification_template');
  }
  drupal_set_message(t('Les notifications ont été mis à jour'));
}

/**
 * formulaire d'ajout d'une notification
 */
function add_notification_template($form, $form_state) {
  $form ['fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configurer la notification'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
  );
  $form ['fieldset'] ['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Titre interne'),
      '#required' => TRUE
  );
  $form ['fieldset'] ['machine_name'] = array(
      '#type' => 'machine_name',
      '#title' => t("Machine Name"),
      '#required' => TRUE,
      '#default_value' => "",
      '#maxlength' => 120,
      '#machine_name' => array(
          'exists' => 'notification_template_name_exists',
          'source' => array(
              'fieldset',
              'title'
          )
      )
  );
  $form ['fieldset'] ['from_mail'] = array(
      '#type' => 'textfield',
      '#title' => t('From email'),
      '#required' => TRUE,
      '#default_value' => variable_get('site_mail')
  );
  $form ['fieldset'] ['email_destination'] = array(
      '#type' => 'textfield',
      '#title' => t('To Email')
  );
  $form ['fieldset'] ['object'] = array(
      '#type' => 'textfield',
      '#title' => t('Objet'),
      '#required' => TRUE
  );
  $form ['fieldset'] ['notification'] = array(
      '#type' => 'text_format',
      '#title' => t('Corps'),
      '#required' => TRUE,
      '#format' => 'filtered_html'
  );
  $form ['fieldset'] ['active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#default_value' => TRUE
  );
  $form ['fieldset'] ['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
  );
  $form ['token_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tokens disponible'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#access' => user_access('voir les tokens')
  );
  $form ['token_fieldset'] ['tokens'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array(
          'node',
          'user',
          'entityform_type',
          'entityform'
      ),
      '#global_types' => TRUE,
      '#click_insert' => TRUE
  );
  return $form;
}
function import_notifications($form, &$form_state){
	$form['fieldset'] = array(
			'#type' => 'fieldset',
			'#title' => t('Importer les notifications'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE
	);
	$form['fieldset']['data'] = array(
		'#type' => 'textarea',
		'#required' => TRUE
	);
	$form['fieldset']['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Importer')
	);
	return $form;
}
function import_notifications_submit($form, &$form_state){
	eval($form_state['values']['data']);
	$updated = 0;
	$created = 0;
	foreach ($data as $machine_name => $notification){
		$notification = (object)$notification;
		if($notification_unchanged = notification_template_load_by_machine_name($machine_name)){
			$updated++;
			$notification->ntid = $notification_unchanged->ntid;
		}
		else{
			$created++;
		}
		db_api_save($notification, 'notification_template');
	}
	drupal_set_message(t('@created ont été créé, @updated ont été mis à jour', array('@created' =>$created, '@updated' => $updated)));
}
/**
 * fonction de submit d'une formulaire de d'ajoute d'une notification
 */
function add_notification_template_submit($form, &$form_state) {
  $values = $form_state ['values'];
  $notif = $values ['notification'] ['value'];
  $format = $values ['notification'] ['format'];
  $notification = (object) $values;
  $notification->notification = $notif;
  $notification->notification_format = $format;
  db_api_save($notification, 'notification_template');
  $form_state ['redirect'] = 'admin/config/notification-templates';
}
